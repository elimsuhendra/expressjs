const express = require("express");
const router = express.Router();
const User = require("../../models/User");
const bcrypt = require("bcryptjs");
var bodyParser = require('body-parser')
// const validateLoginInput = require("../../validation/login");
const validateRegisterInput = require("../../validation/register");

// var cors = require('./cors')

// app.use(cors())

// let allowCrossDomain = function(req, res, next) {
//   res.header('Access-Control-Allow-Origin', "*");
//   res.header('Access-Control-Allow-Headers', "*");
//   next();
// }
// app.use(allowCrossDomain);

// create application/json parser
var jsonParser = bodyParser.json()

// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false })

console.log("users.js");

router.post("/register", urlencodedParser, function (req, res) {
    // return res.status(400).json("post");
    const {errors,isValid} = validateRegisterInput(req.body);

    if(!isValid){
        return res.status(400).json(errors);
    }
    User.findOne({email: req.body.email})
        .then(user => {
            if(user){
                return res.status(400).json({'email' : 'Alamat email sudah digunakan'});
            }else{
                const newUser = new User({
                    name : req.body.name,
                    email : req.body.email,
                    password : req.body.password
                });
                bcrypt.genSalt(10,(err,salt) => {
                    bcrypt.hash(newUser.password,salt,(err,hash) => {
                        if(err) throw err;
                        newUser.password = hash;
                        newUser.save()
                            .then(user => res.json(user))
                            .catch(err => console.log(err))
                        return res.json(newUser);
                    })
                });
            }
        })
});

module.exports = router;