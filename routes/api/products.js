const express = require("express");
const router = express.Router();
const Product = require("../../models/Product");
const bcrypt = require("bcryptjs");
var bodyParser = require('body-parser')

// var cors = require('./cors')

// app.use(cors())

// let allowCrossDomain = function(req, res, next) {
//   res.header('Access-Control-Allow-Origin', "*");
//   res.header('Access-Control-Allow-Headers', "*");
//   next();
// }
// app.use(allowCrossDomain);

// create application/json parser
var jsonParser = bodyParser.json()

// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false })

console.log("product.js");

router.post("/", urlencodedParser, function (req, res) {
    // console.log(req.body);
    // return res.status(200).json(req.body);
    let product = new Product({
        name : req.body.name,
        price : req.body.price,
        image : req.body.image
    });

    product.save(function (err) {
        if (err) {
             return res.status(404).json('Product Created Failed')
        }else{
            return res.status(200).json(product)
        }
    })
});

router.get("/:id", function (req, res) {
    // return res.status(200).send(req.params.id);
    Product.findById(req.params.id, function (err, product) {
        if (err){
            return res.status(404).send("Data not found");
        }else{
            // let data = {
            //     "data":product
            // };
            return res.status(200).json(product);
        }
    })
});

router.put("/:id", function (req, res) {
    // return res.status(200).send(req.params.id);

    Product.findByIdAndUpdate(req.params.id, {$set: req.body}, function (err, product) {
        if (err){
            return res.status(404).send("Data not found");
        }else{
            // let data = {
            //     "data":product
            // };
            return res.status(200).json(product);
        }
    });
});

router.delete("/:id", function (req, res) {
    // return res.status(200).send(req.params.id);

    Product.findByIdAndRemove(req.params.id, function (err) {
        if (err){
            return res.status(404).send("Data not found");
        }else{
            // let data = {
            //     "data":[req.params.id]
            // };
            return res.status(200).json(req.params.id);
        }
    })
});

module.exports = router;