// business.route.js
const express = require("express");
// const router = express.Router();
const Tables = require("../../models/Tables");
const bcrypt = require("bcryptjs");
var bodyParser = require('body-parser')

// const express = require('express');
const tablesRoutes = express.Router();

// Require Business model in our routes module
// let Business = require('./business');

// Defined store route
tablesRoutes.route('/add').post(function (req, res) {
  let tables = new Tables(req.body);
  tables.save()
    .then(tables2 => {
      res.status(200).json({'message': 'Insert data table success !','data':tables2});
      // Tables.find(function(err, data){
      //   if(err){
      //     // console.log(err);
      //     res.status(400).json({'message': 'Insert data failed !','data':[]});
      //   }else{
      //     // res.json(data);
      //     res.status(200).json({'message': 'Insert data table success !','data':data});

      //   }
      // })
    })
    .catch(err => {
      res.status(400).json({'message': 'Insert data failed !','err':err,'data':''});
    });
});

// Defined get data(index or listing) route
tablesRoutes.route('/').get(function (req, res) {
    Tables.find(function(err, data){
    if(err){
      console.log(err);
    }
    else {
      res.json(data);
    }
  });
});

tablesRoutes.post("/edit", function (req, res) {
    // return res.status(200).send(req.body.id);

    Tables.findByIdAndUpdate(req.body.id, {$set: req.body}, function (err, res2) {
        if (err){
            return res.status(404).send("Data not found");
        }else{
            // let data = {
            //     "data":res2
            // };
            // return res.status(200).json(res2);

            Tables.findById(req.body.id, function (err, res3) {
                if (err){
                    return res.status(404).send({'message': 'Edit data failed !','data':""});
                }else{
                    // let data = {
                    //     "data":product
                    // };
                    return res.status(200).json({'message': 'Edit data table success !','data':res3});
                }
            })
        }
    });
});

tablesRoutes.post("/delete", function (req, res) {
    // return res.status(200).send(req.params.id);

    Tables.findByIdAndRemove(req.body.id, function (err) {
        if (err){
            return res.status(404).send({'message': 'Deleted data failed !','data':""});
        }else{
            // let data = {
            //     "data":[req.params.id]
            // };
            return res.status(200).json({'message': 'Delete data table success !','data':req.body.id});
        }
    })
});

module.exports = tablesRoutes;