const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const app = express();
const db = require("./config/keys").mongoURI;
const dbName = 'irvins';
const users = require("./routes/api/users");
const products = require("./routes/api/products");
const business = require("./routes/api/business");
const tables = require("./routes/api/tables");
const path = require('path');

// create application/json parser
var jsonParser = bodyParser.json()

// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false })

var cors = require('cors')

app.use(cors())
app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());

mongoose
   .connect(db)
   .then(() => console.log("mongoDB Connected"))
    .catch((err) => console.log(err));

//routes
app.use(express.static('public'))
// console.log(users);
app.use('/api/users',users);
app.use('/api/products',products);
app.use('/api/business',business);
app.use('/api/tables',tables);
app.get('/', (req, res) => res.send('Hello World!'))
// app.post('/api/products', urlencodedParser, function (req, res) {
// 	res.status(201).send('welcome, ' + req.body._id)
// })

const port = process.env.PORT || 4000;

app.listen(port, () => console.log("server running on port "+port));