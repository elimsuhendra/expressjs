const mongoose = require("mongoose");
const schema = mongoose.Schema;

const ProductSchema = new schema({
    name:{
        type:String,
        required:true
    },
    price:{
        type:String,
        required:true
    },
    image:{
        type:String,
        required:true
    }
});

module.exports = User = mongoose.model("product",ProductSchema);