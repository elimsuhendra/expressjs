const mongoose = require("mongoose");
const schema = mongoose.Schema;

const Tables = new schema({
    name:{
        type:String,
        required:true
    },
    category:{
        type:String,
        required:true
    },
    availability:{
        type:String,
        required:true
    },
    arrival:{
        type:String,
        required:true
    }
});

module.exports = mongoose.model("table",Tables);